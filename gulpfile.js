var gulp = require('gulp');
var cp = require('child_process');
var rename = require('gulp-rename');
var path = require('path');

/* API */

gulp.task('restore', function() {
  return gulp.src('./data/restore.json')
    .pipe(rename('db.json'))
    .pipe(gulp.dest('./data'));
});

gulp.task('serve', ['restore'], function(done) {
  var serverPath = path
    .join(__dirname, './node_modules/.bin/json-server')
    .replace(/ /g, '\\ ');
  cp.exec(serverPath + ' --watch ./data/db.json --port 3001', {stdio: 'inherit'})
    .on('close', done);
});

gulp.task('default', ['serve']);
