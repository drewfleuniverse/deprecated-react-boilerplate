import * as types from '../actions/action-types';

export function getHomeSuccess(article) {
  return {
    type: types.GET_HOME_SUCCESS,
    article: article
  };
}
