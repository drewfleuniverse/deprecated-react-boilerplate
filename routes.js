import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

// Layouts
import MainLayout from './components/Common/Layouts/MainLayout.jsx';
import SearchLayoutContainer from './containers/SearchLayoutContainer.jsx';

// Pages
import HomeContainer from './containers/HomeContainer.jsx'
import FruitListContainer from './containers/FruitListContainer.jsx';

export default (
  <Router history={browserHistory}>
    <Route component={MainLayout}>
      <Route path="/" component={HomeContainer} />
      <Route path="fruitlist">
        <Route component={SearchLayoutContainer}>
          <IndexRoute component={FruitListContainer} />
        </Route>
      </Route>
    </Route>
  </Router>
);
