import { combineReducers } from 'redux';

// Reducers
import homeReducer from './homeReducer'
import widgetReducer from './widget-reducer';
import searchLayoutReducer from './search-layout-reducer';

// Combine Reducers
var reducers = combineReducers({
  homeState: homeReducer,
  widgetState: widgetReducer,
  searchLayoutState: searchLayoutReducer
});

export default reducers;
