import * as types from '../actions/action-types';

const initialState = {
  article: {}
}

const homeReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.GET_HOME_SUCCESS:
      return Object.assign({}, state, {
        article: action.article
      });

  }

  return state;

}

export default homeReducer;
