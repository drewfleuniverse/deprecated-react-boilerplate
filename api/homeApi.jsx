import axios from 'axios';
import store from '../store';
import { getHomeSuccess } from '../actions/homeActions';

export function getHome() {
  return axios.get('http://localhost:3001/home')
    .then(response => {
      store.dispatch(getHomeSuccess(response.data));
      return response;
    });
}
