import axios from 'axios';
import store from '../store';
import { getWidgetsSuccess, deleteWidgetSuccess } from '../actions/widget-actions';

/**
 * Get widgets
 */

export function getFruits() {
  return axios.get('http://localhost:3001/widgets')
    .then(response => {
      store.dispatch(getWidgetsSuccess(response.data));
      return response;
    });
}

/**
 * Search Widgets
 */

export function searchFruits(query = '') {
  return axios.get('http://localhost:3001/widgets?q='+ query)
    .then(response => {
      store.dispatch(getWidgetsSuccess(response.data));
      return response;
    });
}

/**
 * Delete a widget
 */

export function deleteFruit(widgetId) {
  return axios.delete('http://localhost:3001/widgets/' + widgetId)
    .then(response => {
      store.dispatch(deleteWidgetSuccess(widgetId));
      return response;
    });
}
