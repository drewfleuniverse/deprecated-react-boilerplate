import { Link } from 'react-router';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import css from './MainLayout.scss';

class MainLayout extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app">

        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">My App</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <LinkContainer to="/">
                <NavItem eventKey={0}>Home</NavItem>
              </LinkContainer>
              <LinkContainer to="/fruitlist">
                <NavItem eventKey={1}>Fruit List</NavItem>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div className="container-fluid">
          <div className="row">
            <aside className={`col-md-2 ${css.sidebar}`}>
              <ul>
                <li><Link to="/" activeClassName="active">Home</Link></li>
                <li><Link to="/fruitlist" activeClassName="active">Fruit List</Link></li>
              </ul>
            </aside>
            <main className="col-md-10">
              <div className="row">
                {this.props.children}
              </div>
            </main>
          </div>
        </div>

      </div>
    );
  }
}

export default MainLayout;
