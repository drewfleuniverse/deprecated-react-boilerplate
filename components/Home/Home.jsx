import React from 'react';
import Jumbotron from './Jumbotron/Jumbotron';
import css from './Home.scss';

const Home = (props) => {
  const {article} = props;

  return (
    <div className={`col-md-10 ${css.home}`}>
      <Jumbotron article={article}/>
      <p></p>
    </div>
  )
}

export default Home;
