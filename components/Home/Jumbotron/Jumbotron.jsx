import React from 'react';

import css from './Jumbotron.scss';

const Jumbotron = (props) => {
  const {article} = props;

  return (
    <div className={`jumbotron ${css.aquaJumbotron}`}>
        <h1>
          {article.title}
        </h1>
        <p className="lead">
          {article.content}
        </p>
    </div>
  )
}

export default Jumbotron;
