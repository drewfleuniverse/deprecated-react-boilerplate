import React from 'react';
import { connect } from 'react-redux';
import SearchLayout from '../components/Common/Layouts/SearchLayout.jsx';

const mapStateToProps = function(store) {
  let searchType = store.searchLayoutState.searchType;
  let totalResults = 0;

  if (searchType === 'widgets') {
    totalResults = store.widgetState.widgets.length;
  }

  return {
    searchType,
    title: store.searchLayoutState.title,
    totalResults
  };

};

export default connect(mapStateToProps)(SearchLayout);
