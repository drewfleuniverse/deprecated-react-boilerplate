import React from 'react';
import { connect } from 'react-redux';
import Home from '../components/Home/Home.jsx';
import * as homeApi from '../api/homeApi.jsx';
import store from '../store';

class HomeContainer extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    homeApi.getHome();
  }

  render() {
    return (
      <Home article={this.props.article} />
    );
  }
}

const mapStateToProps = function(store) {
  return {
    article: store.homeState.article
  };
};

export default connect(mapStateToProps)(HomeContainer);
