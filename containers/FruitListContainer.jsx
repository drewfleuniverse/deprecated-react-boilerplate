import React from 'react';
import { connect } from 'react-redux';
import FruitList from '../components/FruitList/FruitList.jsx';
import * as fruitListApi from '../api/fruitListApi.jsx';
import store from '../store';
import { loadSearchLayout } from '../actions/search-layout-actions';

const FruitListContainer = React.createClass({

  componentDidMount: function() {
    fruitListApi.getFruits();
    store.dispatch(loadSearchLayout('widgets', 'Widget Results'));
  },

  render: function() {
    return (
      <FruitList widgets={this.props.widgets} deleteWidget={fruitListApi.deleteFruit} />
    );
  }

});

const mapStateToProps = function(store) {
  return {
    widgets: store.widgetState.widgets
  };
};

export default connect(mapStateToProps)(FruitListContainer);
